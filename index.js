// 3.0 Getting all the data on todo list

fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((json) => {
        console.log(json);
        // 4.0 Retrieved titles using map()
        let dataRetrieve = [];
        dataRetrieve.push(json);
        let title = dataRetrieve.map((data) => {
            return data.map((data) => {
                return data.title;
            });
        });
        console.log(title);
    });

// 5.0 Retrieve single to do list item
fetch("https://jsonplaceholder.typicode.com/todos/1").then((response) =>
    response.json().then((json) => {
        console.log(json);
        // 6.0 Retrieve Title and Status of an todo item
        console.log(
            `The item "${json.title}" on the list item has a status of ${json.completed}`
        );
    })
);
// 7.0 Creating a to do list item using POST
fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
        "Content-Type": "application/json",
    },
    body: JSON.stringify({
        title: "Created To Do List Items",
        completed: false,
        userId: 1,
    }),
})
    .then((response) => response.json())
    .then((json) => console.log(json));

// 8.0 & 9.0 Updating a to do list item using PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-Type": "application/json",
    },
    body: JSON.stringify({
        title: "Updated To Do List Items",
        dateCompleted: "Pending",
        description:
            "To update my todo list with a diffrent list data structure",
        status: "Pending",

        userId: 1,
    }),
})
    .then((response) => response.json())
    .then((json) => console.log(json));

// 10.0 & 11.0  Updating using PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json",
    },
    body: JSON.stringify({
        dateCompleted: "02/06/2023",
        status: "Completed",
        completed: true,
    }),
})
    .then((response) => response.json())
    .then((json) => console.log(json));

// 12.0 Will delete a todo list item
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE",
});
